﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmUpdateTiGia : DevExpress.XtraEditors.XtraForm
    {
        DACK_ware5Entities1 dbe = new DACK_ware5Entities1();
        TyGia tg = new TyGia();
        public frmUpdateTiGia()
        {
            InitializeComponent();
        }
        public frmUpdateTiGia(string ma, string ten, string quydoi, string trangthai)
        {
            InitializeComponent();
            LoadTextBox(ma, ten, quydoi, trangthai);
        }

        private void LoadTextBox(string ma, string ten, string quydoi, string trangthai)
        {
            
                txtMaTiGia.Text = ma;
                txtTenTiGia.Text = ten;
                
                if (trangthai == "True")
                {
                    chkTrangThaiTiGia.Checked = true;
                }
                txtTiGiaQuyDoi.Text = quydoi;
           
            
        }

        private void btnCapNhatTiGia_Click(object sender, EventArgs e)
        {
            string id = txtMaTiGia.Text;
            TyGia tg = dbe.TyGias.Single(n => n.id_TyGia == id);
            tg.TenTyGia = txtTenTiGia.Text;
            tg.TyGiaQuyDoi = decimal.Parse( txtTiGiaQuyDoi.Text);
            if (chkTrangThaiTiGia.Checked == true)
            { tg.TrangThai = true; }
            else
            { tg.TrangThai = false; }
            dbe.SaveChangesAsync();
            MessageBox.Show("Sửa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
            this.Close();

        }

        private void btnXoaTiGia_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmUpdateTiGia_Load(object sender, EventArgs e)
        {

        }
    }
}
