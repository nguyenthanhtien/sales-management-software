﻿using QuanLyBanHang;
using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DACK_ware2
{
    public partial class frmNhaCungCap : Form
    {
        DACK_ware5Entities1 dbe = new DACK_ware5Entities1();
        public frmNhaCungCap()
        {
            InitializeComponent();
        }

        private void btn_Them_Click(object sender, EventArgs e)
        {
            var form = new frmUpdateNCC();
            form.Show();
        }

        private void btn_CapNhat_Click(object sender, EventArgs e)
        {
            
            gc_NCC.DataSource = dbe.NhaCungCaps.ToList();
        }

        private void frmNhaCungCap_Load(object sender, EventArgs e)
        {
    
            gc_NCC.DataSource = dbe.NhaCungCaps.ToList();
        }

        private void btn_Sua_Click(object sender, EventArgs e)
        {
            string id = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "id_NCC").ToString();
            string id_KV = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "id_KV").ToString();
            string TenNCC = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TenNCC").ToString();
            string ChucVu = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "ChucVu").ToString();
            string DiaChi = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "DiaChi").ToString();
            string SDT = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "SDT").ToString();
            string TrangThai = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TrangThai").ToString();
            var form = new frmEditNCC(id,id_KV,TenNCC,ChucVu,DiaChi,SDT,TrangThai);
            form.Show();
        }

        private void btn_Xoa_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.Cancel)
            {
                string ma = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "id_NCC").ToString();
                int id = int.Parse(ma);
                NhaCungCap NCC = dbe.NhaCungCaps.Single(n => n.id_NCC == id);
                dbe.NhaCungCaps.Remove(NCC);
                dbe.SaveChanges();
                gc_NCC.DataSource = dbe.NhaCungCaps.ToList();
            }
        }
    }
}
