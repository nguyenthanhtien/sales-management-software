﻿namespace DACK_ware2
{
    partial class frmNhaCungCap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Them = new DevExpress.XtraEditors.SimpleButton();
            this.gc_NCC = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid_NCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colid_KV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_TenNCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_ChucVu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_DiaChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_SDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_TrangThai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn_Sua = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_CapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Dong = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gc_NCC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Them
            // 
            this.btn_Them.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Them.Location = new System.Drawing.Point(52, 12);
            this.btn_Them.Name = "btn_Them";
            this.btn_Them.Size = new System.Drawing.Size(75, 23);
            this.btn_Them.TabIndex = 0;
            this.btn_Them.Text = "Thêm";
            this.btn_Them.Click += new System.EventHandler(this.btn_Them_Click);
            // 
            // gc_NCC
            // 
            this.gc_NCC.Location = new System.Drawing.Point(12, 42);
            this.gc_NCC.MainView = this.gridView1;
            this.gc_NCC.Name = "gc_NCC";
            this.gc_NCC.Size = new System.Drawing.Size(756, 406);
            this.gc_NCC.TabIndex = 1;
            this.gc_NCC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid_NCC,
            this.colid_KV,
            this.col_TenNCC,
            this.col_ChucVu,
            this.col_DiaChi,
            this.col_SDT,
            this.col_TrangThai});
            this.gridView1.GridControl = this.gc_NCC;
            this.gridView1.Name = "gridView1";
            // 
            // colid_NCC
            // 
            this.colid_NCC.Caption = "Mã Nhà Cung Cấp";
            this.colid_NCC.FieldName = "id_NCC";
            this.colid_NCC.Name = "colid_NCC";
            this.colid_NCC.Visible = true;
            this.colid_NCC.VisibleIndex = 0;
            // 
            // colid_KV
            // 
            this.colid_KV.Caption = "Mã Khu Vực";
            this.colid_KV.FieldName = "id_KV";
            this.colid_KV.Name = "colid_KV";
            this.colid_KV.Visible = true;
            this.colid_KV.VisibleIndex = 1;
            // 
            // col_TenNCC
            // 
            this.col_TenNCC.Caption = "Tên Nhà Cung Cấp";
            this.col_TenNCC.FieldName = "TenNCC";
            this.col_TenNCC.Name = "col_TenNCC";
            this.col_TenNCC.Visible = true;
            this.col_TenNCC.VisibleIndex = 2;
            // 
            // col_ChucVu
            // 
            this.col_ChucVu.Caption = "Chức Vụ";
            this.col_ChucVu.FieldName = "ChucVu";
            this.col_ChucVu.Name = "col_ChucVu";
            this.col_ChucVu.Visible = true;
            this.col_ChucVu.VisibleIndex = 3;
            // 
            // col_DiaChi
            // 
            this.col_DiaChi.Caption = "Địa Chỉ";
            this.col_DiaChi.FieldName = "DiaChi";
            this.col_DiaChi.Name = "col_DiaChi";
            this.col_DiaChi.Visible = true;
            this.col_DiaChi.VisibleIndex = 4;
            // 
            // col_SDT
            // 
            this.col_SDT.Caption = "SĐT";
            this.col_SDT.FieldName = "SDT";
            this.col_SDT.Name = "col_SDT";
            this.col_SDT.Visible = true;
            this.col_SDT.VisibleIndex = 5;
            // 
            // col_TrangThai
            // 
            this.col_TrangThai.Caption = "Trạng Thái";
            this.col_TrangThai.FieldName = "TrangThai";
            this.col_TrangThai.Name = "col_TrangThai";
            this.col_TrangThai.Visible = true;
            this.col_TrangThai.VisibleIndex = 6;
            // 
            // btn_Sua
            // 
            this.btn_Sua.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Sua.Location = new System.Drawing.Point(205, 12);
            this.btn_Sua.Name = "btn_Sua";
            this.btn_Sua.Size = new System.Drawing.Size(75, 23);
            this.btn_Sua.TabIndex = 2;
            this.btn_Sua.Text = "Sửa";
            this.btn_Sua.Click += new System.EventHandler(this.btn_Sua_Click);
            // 
            // btn_Xoa
            // 
            this.btn_Xoa.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Xoa.Location = new System.Drawing.Point(353, 12);
            this.btn_Xoa.Name = "btn_Xoa";
            this.btn_Xoa.Size = new System.Drawing.Size(75, 23);
            this.btn_Xoa.TabIndex = 3;
            this.btn_Xoa.Text = "Xóa";
            this.btn_Xoa.Click += new System.EventHandler(this.btn_Xoa_Click);
            // 
            // btn_CapNhat
            // 
            this.btn_CapNhat.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_CapNhat.Location = new System.Drawing.Point(499, 12);
            this.btn_CapNhat.Name = "btn_CapNhat";
            this.btn_CapNhat.Size = new System.Drawing.Size(75, 23);
            this.btn_CapNhat.TabIndex = 4;
            this.btn_CapNhat.Text = "Cập Nhật";
            this.btn_CapNhat.Click += new System.EventHandler(this.btn_CapNhat_Click);
            // 
            // btn_Dong
            // 
            this.btn_Dong.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Dong.Location = new System.Drawing.Point(624, 12);
            this.btn_Dong.Name = "btn_Dong";
            this.btn_Dong.Size = new System.Drawing.Size(75, 23);
            this.btn_Dong.TabIndex = 5;
            this.btn_Dong.Text = "Đóng";
            // 
            // frmNhaCungCap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 460);
            this.Controls.Add(this.btn_Dong);
            this.Controls.Add(this.btn_CapNhat);
            this.Controls.Add(this.btn_Xoa);
            this.Controls.Add(this.btn_Sua);
            this.Controls.Add(this.gc_NCC);
            this.Controls.Add(this.btn_Them);
            this.Name = "frmNhaCungCap";
            this.Text = "Nhà cung cấp";
            this.Load += new System.EventHandler(this.frmNhaCungCap_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gc_NCC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btn_Them;
        private DevExpress.XtraGrid.GridControl gc_NCC;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colid_NCC;
        private DevExpress.XtraGrid.Columns.GridColumn colid_KV;
        private DevExpress.XtraEditors.SimpleButton btn_Sua;
        private DevExpress.XtraEditors.SimpleButton btn_Xoa;
        private DevExpress.XtraEditors.SimpleButton btn_CapNhat;
        private DevExpress.XtraEditors.SimpleButton btn_Dong;
        private DevExpress.XtraGrid.Columns.GridColumn col_TenNCC;
        private DevExpress.XtraGrid.Columns.GridColumn col_ChucVu;
        private DevExpress.XtraGrid.Columns.GridColumn col_DiaChi;
        private DevExpress.XtraGrid.Columns.GridColumn col_SDT;
        private DevExpress.XtraGrid.Columns.GridColumn col_TrangThai;
    }
}