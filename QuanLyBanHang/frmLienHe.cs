﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmLienHe : Form
    {
        QLBHEntities db = new QLBHEntities();
        
        public frmLienHe()
        {
            InitializeComponent();
            LoadTextBox();
        }

        private void LoadTextBox()
        {
            var n = from m in db.LienHes select new {m.TenDonVi, m.DiaChi, m.SDT, m.Fax, m.website, m.mail, m.linhvuc, m.nganhnghe, m.Id_Thue, m.GPKD, m.NguoiLienHe, m.NoiDung };
            foreach (var k in n)
            {
                txt_TenDonVi.Text = k.TenDonVi;
                txt_DiaChi.Text = k.DiaChi; 
                txt_SDT.Text = k.SDT.ToString(); 
                txt_Fax.Text = k.Fax.ToString(); 
                txt_WebSide.Text = k.website; 
                txt_Mail.Text = k.mail; 
                txt_LinhVuc.Text = k.linhvuc; 
                txt_NganhNghe.Text = k.nganhnghe; 
                txt_MaThue.Text = k.Id_Thue.ToString();
                txt_GPKD.Text = k.GPKD.ToString(); 
                txt_NguoiLienHe.Text = k.NguoiLienHe; 
                txt_NoiDung.Text = k.NoiDung; 
            }


        }
        private void frmLienHe_Load(object sender, EventArgs e)
        {

        }

        private void btn_CapNhat_Click(object sender, EventArgs e)
        {
            string id = txt_TenDonVi.Text;
            LienHe lh = db.LienHes.Single(n => n.TenDonVi == id);
            lh.TenDonVi = txt_TenDonVi.Text;
            lh.DiaChi = txt_DiaChi.Text;
            lh.SDT = int.Parse(txt_SDT.Text);
            lh.Fax = int.Parse(txt_Fax.Text);
            lh.website = txt_WebSide.Text;
            lh.mail = txt_Mail.Text;
            lh.linhvuc = txt_LinhVuc.Text;
            lh.nganhnghe = txt_NganhNghe.Text;
            lh.Id_Thue = int.Parse(txt_MaThue.Text);
            lh.GPKD = int.Parse(txt_GPKD.Text);
            lh.NguoiLienHe = txt_NguoiLienHe.Text;
            lh.NoiDung = txt_NoiDung.Text;
            db.SaveChangesAsync();
            MessageBox.Show("Cập Nhật Thành Công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
            this.Close();
        }

        private void btn_KetThuc_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmLienHe_Load(object sender, EventArgs e)
        {

        }
    }
}
