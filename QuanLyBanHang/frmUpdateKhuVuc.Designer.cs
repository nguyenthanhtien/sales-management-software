﻿namespace QuanLyBanHang
{
    partial class frmUpdateKhuVuc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_TenKV = new System.Windows.Forms.TextBox();
            this.Labal1 = new System.Windows.Forms.Label();
            this.btn_CapNhat = new System.Windows.Forms.Button();
            this.cbQuanLy = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txt_TenKV
            // 
            this.txt_TenKV.Location = new System.Drawing.Point(155, 54);
            this.txt_TenKV.Name = "txt_TenKV";
            this.txt_TenKV.Size = new System.Drawing.Size(146, 20);
            this.txt_TenKV.TabIndex = 2;
            // 
            // Labal1
            // 
            this.Labal1.AutoSize = true;
            this.Labal1.Location = new System.Drawing.Point(33, 57);
            this.Labal1.Name = "Labal1";
            this.Labal1.Size = new System.Drawing.Size(70, 13);
            this.Labal1.TabIndex = 4;
            this.Labal1.Text = "Tên Khu Vực";
            // 
            // btn_CapNhat
            // 
            this.btn_CapNhat.Location = new System.Drawing.Point(112, 140);
            this.btn_CapNhat.Name = "btn_CapNhat";
            this.btn_CapNhat.Size = new System.Drawing.Size(91, 23);
            this.btn_CapNhat.TabIndex = 6;
            this.btn_CapNhat.Text = "Cập Nhật";
            this.btn_CapNhat.UseVisualStyleBackColor = true;
            this.btn_CapNhat.Click += new System.EventHandler(this.btn_CapNhat_Click);
            // 
            // cbQuanLy
            // 
            this.cbQuanLy.AutoSize = true;
            this.cbQuanLy.Location = new System.Drawing.Point(157, 94);
            this.cbQuanLy.Name = "cbQuanLy";
            this.cbQuanLy.Size = new System.Drawing.Size(82, 17);
            this.cbQuanLy.TabIndex = 7;
            this.cbQuanLy.Text = "Còn quản lý";
            this.cbQuanLy.UseVisualStyleBackColor = true;
            // 
            // frmUpdateKhuVuc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 175);
            this.Controls.Add(this.cbQuanLy);
            this.Controls.Add(this.btn_CapNhat);
            this.Controls.Add(this.Labal1);
            this.Controls.Add(this.txt_TenKV);
            this.Name = "frmUpdateKhuVuc";
            this.Text = "UpdateKhuVuc";
            this.Load += new System.EventHandler(this.frmUpdateKhuVuc_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txt_TenKV;
        private System.Windows.Forms.Label Labal1;
        private System.Windows.Forms.Button btn_CapNhat;
        private System.Windows.Forms.CheckBox cbQuanLy;
    }
}