﻿namespace QuanLyBanHang
{
    partial class frmEditKhuVuc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_TenKV = new System.Windows.Forms.TextBox();
            this.btn_Sửa = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_ID = new System.Windows.Forms.TextBox();
            this.cbQuanLy = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tên Khu Vực";
            // 
            // txt_TenKV
            // 
            this.txt_TenKV.Location = new System.Drawing.Point(149, 37);
            this.txt_TenKV.Name = "txt_TenKV";
            this.txt_TenKV.Size = new System.Drawing.Size(100, 20);
            this.txt_TenKV.TabIndex = 2;
            // 
            // btn_Sửa
            // 
            this.btn_Sửa.Location = new System.Drawing.Point(104, 174);
            this.btn_Sửa.Name = "btn_Sửa";
            this.btn_Sửa.Size = new System.Drawing.Size(75, 23);
            this.btn_Sửa.TabIndex = 4;
            this.btn_Sửa.Text = "Lưu";
            this.btn_Sửa.UseVisualStyleBackColor = true;
            this.btn_Sửa.Click += new System.EventHandler(this.btn_Sửa_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "ID";
            // 
            // txt_ID
            // 
            this.txt_ID.Location = new System.Drawing.Point(149, 139);
            this.txt_ID.Name = "txt_ID";
            this.txt_ID.Size = new System.Drawing.Size(100, 20);
            this.txt_ID.TabIndex = 3;
            // 
            // cbQuanLy
            // 
            this.cbQuanLy.AutoSize = true;
            this.cbQuanLy.Location = new System.Drawing.Point(150, 93);
            this.cbQuanLy.Name = "cbQuanLy";
            this.cbQuanLy.Size = new System.Drawing.Size(82, 17);
            this.cbQuanLy.TabIndex = 5;
            this.cbQuanLy.Text = "Còn quản lý";
            this.cbQuanLy.UseVisualStyleBackColor = true;
            // 
            // frmEditKhuVuc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 209);
            this.Controls.Add(this.cbQuanLy);
            this.Controls.Add(this.btn_Sửa);
            this.Controls.Add(this.txt_ID);
            this.Controls.Add(this.txt_TenKV);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "frmEditKhuVuc";
            this.Text = "frmEditKhuVuc";
            this.Load += new System.EventHandler(this.frmEditKhuVuc_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_TenKV;
        private System.Windows.Forms.Button btn_Sửa;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_ID;
        private System.Windows.Forms.CheckBox cbQuanLy;
    }
}