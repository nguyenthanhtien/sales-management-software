﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmEditKhuVuc : Form
    {
        DACK_ware5Entities1 dbo = new DACK_ware5Entities1();
        public frmEditKhuVuc(string id,string ten, string TT)
        {
            InitializeComponent();
            LoadTextBox(id,ten, TT);
            txt_ID.Enabled = false;
        }
        private void LoadTextBox(string id,string ten, string trangthai)
        {
            txt_ID.Text = id;
            txt_TenKV.Text = ten;
            if (trangthai == "True")
            {
                cbQuanLy.Checked = true;
            }
           
        }

        private void btn_Sửa_Click(object sender, EventArgs e)
        {
            int id = int.Parse(txt_ID.Text);
            KhuVuc kv = dbo.KhuVucs.Single(n => n.Id_KhuVuc == id);
            kv.TenKhuVuc = txt_TenKV.Text;
            if (cbQuanLy.Checked == true)
            {
                kv.TrangThai = true;
            }
            else
            {
                kv.TrangThai = false;
            }
            dbo.SaveChangesAsync();
            MessageBox.Show("Sửa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
            this.Close();
        }

        private void frmEditKhuVuc_Load(object sender, EventArgs e)
        {

        }
    }
}
