﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmSuaNhanVien : Form
    {
        DACK_ware5Entities1 dbe = new DACK_ware5Entities1();
        public frmSuaNhanVien(string  id, string ten, string diachi, string SDT, string email, string trangthai)
        {
            InitializeComponent();
            LoadData(id, ten, diachi, SDT, email, trangthai);
        }

        private void LoadData(string id, string ten, string diachi, string SDT, string email, string trangthai)

        {
            txtMaNhanVien.Text = id;
            txtTenNhanVien.Text = ten;
            txtDiaChi.Text = diachi;
            txtSDT.Text = SDT;
            txtEmail.Text = email;
           if(trangthai == "true")
            { cbConQuanLy.Checked = true; }


        }

        private void btnLuu_Click(object sender, EventArgs e)
        {

            int id = int.Parse(txtMaNhanVien.Text);
            NhanVien nhanvien = dbe.NhanViens.Single(n => n.id_NhanVien == id);
            nhanvien.TenNV = txtTenNhanVien.Text;
            nhanvien.DiaChi = txtDiaChi.Text;
            nhanvien.SDT = txtSDT.Text;
            if (cbConQuanLy.Checked == true)
            { nhanvien.trangthai = true; }
            else { nhanvien.trangthai = false; }
            nhanvien.DiaChi = txtDiaChi.Text;
            dbe.SaveChangesAsync();
            MessageBox.Show("Sửa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
        }

        private void txtMaNhanVien_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbConQuanLy_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void frmSuaNhanVien_Load(object sender, EventArgs e)
        {

        }
    }
}
