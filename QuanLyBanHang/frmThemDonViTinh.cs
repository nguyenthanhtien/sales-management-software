﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmThemDonViTinh : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
     
        public frmThemDonViTinh()
        {
            InitializeComponent();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DonViTinh bp = new DonViTinh();
            bp.TenDonVi = txtTen.Text;
            bp.GhiChu = txtGhiChu.Text;
            if (cbConQuanLy.Checked == true)
            {
                bp.TrangThai = true;
            }
            else
            {
                bp.TrangThai = false;
            }
            

            db.DonViTinhs.Add(bp);
            db.SaveChanges();
            MessageBox.Show("Thêm thành công !!");
            this.Close();
        }
    }
}
