﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmDonViTinh : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
        public frmDonViTinh()
        {
            InitializeComponent();
            LoadDuLieu();
        }
        private void LoadDuLieu()
        {
            gc_DonViTinh.DataSource = db.DonViTinhs.ToList();
        }

        private void gc_DonViTinh_Click(object sender, EventArgs e)
        {

        }

        private void btn_Dong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_CapNhat_Click(object sender, EventArgs e)
        {
            LoadDuLieu();
        }

        private void btn_Xoa_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.Cancel)
            {
                string maDV = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Id_DV").ToString();
                int id = int.Parse(maDV);
                DonViTinh donvitinh = db.DonViTinhs.Single(n => n.Id_DV == id);
                db.DonViTinhs.Remove(donvitinh);
                db.SaveChanges();
                LoadDuLieu();

            }
            else
            {
                MessageBox.Show("Không xoá");
            }
        }

        private void btn_Them_Click(object sender, EventArgs e)
        {
            var form = new frmThemDonViTinh();
            form.Show();
            LoadDuLieu();
        }

        private void btn_Sua_Click(object sender, EventArgs e)
        {
            string id = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Id_DV").ToString();
            string ten = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TenDonVi").ToString();
            string ghichu = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GhiChu").ToString();
            string trangthai = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TrangThai").ToString();
            var form = new frmSuaDonViTinh(id, ten, ghichu, trangthai);
            form.Show();
            LoadDuLieu();
        }
    }
}
