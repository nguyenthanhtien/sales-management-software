﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmEditKhachHang : Form
    {
        DACK_ware5Entities1 dbe = new DACK_ware5Entities1();
        public frmEditKhachHang(string id, string id_kv, string TenKH, string DiaChi, string SĐT, string Email, string Web, string MaThue, string SoTK, string TenNganHang, string TrangThai)
        {
            InitializeComponent();
            LoadComboBox();
            LoadTextBox(id, id_kv, TenKH, DiaChi, SĐT, Email, Web, MaThue, SoTK, TenNganHang, TrangThai);
            txt_idkh.Enabled = false;

        }
        private void LoadComboBox()
        {
            var a = dbe.KhuVucs.ToList();
            lue_KV.Properties.DataSource = a.ToList();
            lue_KV.Properties.DisplayMember = "TenKhuVuc";
            lue_KV.Properties.ValueMember = "Id_KhuVuc";
            

        }
        private void LoadTextBox(string id,string id_kv,string TenKH,string DiaChi,string SĐT, string Email,string Web, string MaThue, string SoTK, string TenNganHang, string TrangThai)
        {
            txt_idkh.Text = id;
            lue_KV.Text = id_kv;
            txt_TenKH.Text = TenKH;
            txt_DiaChi.Text = DiaChi;
            txt_SDT.Text = SĐT;
            txt_Email.Text = Email;
            txt_Website.Text = Web;
            txt_MST.Text = MaThue;
            txt_SoTK.Text = SoTK;
            txt_TenNganHang.Text = TenNganHang;
            if (TrangThai == "True")
            {
                cbQuanLy.Checked = true;
            }
          

        }

        private void frmEditKhachHang_Load(object sender, EventArgs e)
        {

        }

        private void btn_Luu_Click(object sender, EventArgs e)
        {
            int id = int.Parse(txt_idkh.Text);
            KhachHang kh = dbe.KhachHangs.Single(n => n.Id_KH == id);
            string m = lue_KV.EditValue.ToString();
            kh.Id_KhuVuc = int.Parse(m);
            kh.TenKhachHang = txt_TenKH.Text;
            kh.DiaChi = txt_DiaChi.Text;
            kh.SĐT = txt_SDT.Text;
            kh.Email = txt_Email.Text;
            kh.Website = txt_Website.Text;
            kh.MaSoThue = Int32.Parse(txt_MST.Text);
            kh.SoTK = Int32.Parse(txt_SoTK.Text);
            kh.TenNganHang = txt_TenNganHang.Text;
            if (cbQuanLy.Checked == true)
            {
                kh.TrangThai = true;
            }
            else
            {
                kh.TrangThai = false;
            }
            dbe.SaveChangesAsync();
            MessageBox.Show("Sửa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
            this.Close();
        }
    }
}
