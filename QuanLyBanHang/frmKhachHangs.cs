﻿using QuanLyBanHang;
using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DACK_ware2
{
    public partial class frmKhachHang : Form
    {
        DACK_ware5Entities1 dbe = new DACK_ware5Entities1();
        public frmKhachHang()
        {
            InitializeComponent();

        }

        private void frmKhachHang_Load(object sender, EventArgs e)
        {
           
            gc_KhachHang.DataSource = dbe.KhachHangs.ToList();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
         
            gc_KhachHang.DataSource = dbe.KhachHangs.ToList();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            var form = new frmUpdateKH();
            form.Show();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            string id_KH = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Id_KH").ToString();
            string ID_KV = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Id_KhuVuc").ToString();
            string TenKhachHang = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TenKhachHang").ToString();
            string DiaChi = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "DiaChi").ToString();
            string SĐT = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "SĐT").ToString();
            string Email = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Email").ToString();
            string Website = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Website").ToString();
            string MaSoThue = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MaSoThue").ToString();
            string SoTK = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "SoTK").ToString();
            string TenNganHang = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TenNganHang").ToString();
            string TrangThai = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TrangThai").ToString();
            var form = new frmEditKhachHang(id_KH,ID_KV,TenKhachHang,DiaChi,SĐT,Email,Website,MaSoThue,SoTK,TenNganHang,TrangThai);
            form.Show();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.Cancel)
            {
                string ma = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Id_KH").ToString();
                int id = int.Parse(ma);
                KhachHang kh = dbe.KhachHangs.Single(n => n.Id_KH == id);
                dbe.KhachHangs.Remove(kh);
                dbe.SaveChanges();
               gc_KhachHang.DataSource = dbe.KhachHangs.ToList();
            }
        }
    }
}
