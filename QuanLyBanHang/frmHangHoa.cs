﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace QuanLyBanHang
{
    public partial class frmHangHoa : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
        public frmHangHoa()
        {
            InitializeComponent();
            LoadDanhSachHangHoa();
        }

        private void LoadDanhSachHangHoa()
        {
            var query = from n in db.HangHoas from m in db.NhomHangs from k in db.DonViTinhs from p in db.Khoes
                        where n.id_nhomhang == m.Id_NhomHang && n.id_DonVi == k.Id_DV && p.Id_Kho == n.id_kho
                        select new {n.id_hanghoa, p.TenKho ,k.TenDonVi, n.GiaBanLe, n.GiaMua, n.GiaBanSi, n.Tenhang , n.ToiThieu, n.TinhChat, n.TrangThai, m.TenNhomHang};

            foreach (var item in query)
            {
                var result = query.ToList();

                grdDanhSachHangHoa.DataSource = result;

            }

        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            var form = new frmThemDanhSachHangHoa();
            form.Show();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.Cancel)
            {
                string ma = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "id_hanghoa").ToString();
                int id = int.Parse(ma);
                HangHoa kho = db.HangHoas.Single(n => n.id_hanghoa == id);
                db.HangHoas.Remove(kho);
                db.SaveChanges();
                LoadDanhSachHangHoa();
                MessageBox.Show("Đã xóa", "Thông báo");
            }
        }

        private void btnNapLai_Click(object sender, EventArgs e)
        {
            LoadDanhSachHangHoa();
        }

        private void btnXuat_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = "C:";
            saveFileDialog1.Title = "Save as Excle File";
            saveFileDialog1.FileName = "";
            saveFileDialog1.Filter = "Excle File(2003)|*.xls|Excle File(2007)|*.xlsx";
            if (saveFileDialog1.ShowDialog() != DialogResult.Cancel)
            {
                grdDanhSachHangHoa.ExportToXls(saveFileDialog1.FileName);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string loaihanghoa = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TinhChat").ToString(); 
            string kho = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TenKho").ToString(); 
            string loai = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TenNhomHang").ToString(); 
            string mahang= gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "id_hanghoa").ToString();          
            string tenhang= gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Tenhang").ToString(); 
            string donvi= gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TenDonVi").ToString(); 
            string giamua= gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GiaMua").ToString(); 
            string giabanle= gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GiaBanLe").ToString(); 
            string giabansi= gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GiaBanSi").ToString(); 
            string trangthai= gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TrangThai").ToString(); 

            var form = new frmSuaDanhSachHangHoa( loaihanghoa,  kho,  loai,  mahang, tenhang,  donvi,  giamua,  giabanle,  giabansi,  trangthai);
            form.Show();
        
        }
    }
}
