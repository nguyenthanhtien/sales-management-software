﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmSuaDanhSachHangHoa : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
        public frmSuaDanhSachHangHoa(string loaihanghoa, string kho, string loai, string mahang, string tenhang, string donvi, string giamua, string giabanle, string giabansi, string trangthai)
        {
            InitializeComponent();
            LoadData(loaihanghoa, kho, loai, mahang, tenhang, donvi, giamua, giabanle, giabansi, trangthai);

        }

        private void frmSuaDanhSachHangHoa_Load(object sender, EventArgs e)
        {
            lueDonVi.Properties.DataSource = db.DonViTinhs.ToList();
            lueDonVi.Properties.DisplayMember = "TenDonVi";
            lueDonVi.Properties.ValueMember = "Id_DV";

            lueTenKho.Properties.DataSource = db.Khoes.ToList();
            lueTenKho.Properties.DisplayMember = "TenKho";
            lueTenKho.Properties.ValueMember = "Id_Kho";

            lueTinhChat.Properties.DataSource = db.HangHoas.ToList();
            lueTinhChat.Properties.DisplayMember = "TinhChat";
            lueTinhChat.Properties.ValueMember = "TinhChat";

            LuePhanLoai.Properties.DataSource = db.NhomHangs.ToList();
            LuePhanLoai.Properties.DisplayMember = "TenNhomHang";
            LuePhanLoai.Properties.ValueMember = "Id_NhomHang";
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            int makho =int.Parse(txtMaHang.Text);
            HangHoa hh = db.HangHoas.Single(n=> n.id_hanghoa== makho);
            hh.GiaMua = decimal.Parse(txtGiaMua.Text);
            hh.GiaBanLe = decimal.Parse(txtGiaBanLe.Text);
            hh.GiaBanSi = decimal.Parse(txtGiabanSi.Text);
            hh.Tenhang =  txtTenHang.Text ;
            if (cbQuanLy.Checked == true)
            {
                hh.TrangThai = true;
            }
            db.SaveChangesAsync();
            MessageBox.Show("Sửa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
        }
        private void LoadData(string loaihanghoa, string kho, string loai, string mahang, string tenhang, string donvi, string giamua, string giabanle, string giabansi, string trangthai)
        {
            int tt = int.Parse(trangthai);
            if (tt == 1)
            {
                cbQuanLy.Checked = true;
            }
            txtGiaBanLe.Text = giabanle;
            txtGiabanSi.Text = giabansi;
            txtGiaMua.Text = giamua;
            txtMaHang.Text = mahang;
            txtTenHang.Text = tenhang;
           
        }
    }
}
