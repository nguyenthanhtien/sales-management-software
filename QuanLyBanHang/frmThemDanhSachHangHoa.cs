﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmThemDanhSachHangHoa : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
        public frmThemDanhSachHangHoa()
        {
            InitializeComponent();
            LoadComboBox();
        }
          
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            var hh = new HangHoa();
            hh.TinhChat = lueTinhChat.EditValue.ToString();
            hh.id_kho =int.Parse( lueTenKho.EditValue.ToString());
            hh.id_nhomhang = int.Parse(LuePhanLoai.EditValue.ToString());
            hh.Tenhang = txtTenHang.Text;
            hh.id_DonVi = int.Parse(lueDonVi.EditValue.ToString());
            hh.ToiThieu =int.Parse( txtTonKhoToiThieu.Text);
            hh.GiaMua = int.Parse(cbGiaMua.Text);
            hh.GiaBanLe = int.Parse(cbGiaBanLe.Text);
            hh.GiaBanSi = int.Parse(cbGiaBanSi.Text);
            if (cbQuanLy.Checked == true)
            {
                hh.TrangThai = true;
            }
            else
            { hh.TrangThai = false; }
            db.HangHoas.Add(hh);
            db.SaveChangesAsync();
            MessageBox.Show("Thêm thành công", "Thông báo");
            this.Close();
        }
        private void LoadComboBox()
        {
            lueDonVi.Properties.DataSource = db.DonViTinhs.ToList();
            lueDonVi.Properties.DisplayMember = "TenDonVi";
            lueDonVi.Properties.ValueMember = "Id_DV";

            lueTenKho.Properties.DataSource = db.Khoes.ToList();
            lueTenKho.Properties.DisplayMember = "TenKho";
            lueTenKho.Properties.ValueMember = "Id_Kho";

            lueTinhChat.Properties.DataSource = db.HangHoas.ToList();
            lueTinhChat.Properties.DisplayMember = "TinhChat";
            lueTinhChat.Properties.ValueMember = "TinhChat";

            LuePhanLoai.Properties.DataSource = db.NhomHangs.ToList();
            LuePhanLoai.Properties.DisplayMember = "TenNhomHang";
            LuePhanLoai.Properties.ValueMember = "Id_NhomHang";
        }

        private void frmThemDanhSachHangHoa_Load(object sender, EventArgs e)
        {
           
        }
    }
}
