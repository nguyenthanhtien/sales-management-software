﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmdangnhap : Form
    {
        DACK_ware5Entities1 dbe = new DACK_ware5Entities1();
        public frmdangnhap()
        {
            InitializeComponent();
        }

        private void frmdangnhap_Load(object sender, EventArgs e)
        {

        }
        private void XoaTrang()
        {
            txt_Pass.Text = string.Empty;
            txt_DangNhap.Text = string.Empty;
        }

        private void btn_DangNhap_Click(object sender, EventArgs e)
        {


            if (txt_DangNhap.Text != string.Empty || txt_Pass.Text != string.Empty)
            {
                var user = dbe.NguoiDungs.FirstOrDefault(a => a.Username.Equals(txt_DangNhap.Text));
                if (user != null)
                {
                    user = dbe.NguoiDungs.FirstOrDefault(a => a.Password.Equals(txt_Pass.Text));
                    if (user != null)
                    {
                        var form = new Form1();
                        this.Hide();
                        form.ShowDialog();
                        this.Close();

                    }
                    else
                    {
                        MessageBox.Show("Password error!");
                        XoaTrang();
                    }
                
                }
                else
                {
                    MessageBox.Show("UserName error!");
                    XoaTrang();
                }
            }
            else
            {
                MessageBox.Show("Not null!");
                XoaTrang();
            }
            
        }

    }
}