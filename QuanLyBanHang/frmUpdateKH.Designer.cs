﻿namespace QuanLyBanHang
{
    partial class frmUpdateKH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_tenkhachhang = new System.Windows.Forms.TextBox();
            this.txt_DiaChi = new System.Windows.Forms.TextBox();
            this.txt_SDT = new System.Windows.Forms.TextBox();
            this.txt_Website = new System.Windows.Forms.TextBox();
            this.txt_IDThue = new System.Windows.Forms.TextBox();
            this.txt_sotk = new System.Windows.Forms.TextBox();
            this.txt_TenNganHang = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.btn__CapNhat = new System.Windows.Forms.Button();
            this.lue_KV = new DevExpress.XtraEditors.LookUpEdit();
            this.cbQuanLy = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.lue_KV.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "ID_KV";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tên Khách Hàng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(277, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Website";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(277, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Mã Số Thuế";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(259, 176);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Tên Ngân Hàng";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 126);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Địa Chỉ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 173);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "SĐT";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(277, 221);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Email";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(277, 129);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Số TK";
            // 
            // txt_tenkhachhang
            // 
            this.txt_tenkhachhang.Location = new System.Drawing.Point(98, 80);
            this.txt_tenkhachhang.Name = "txt_tenkhachhang";
            this.txt_tenkhachhang.Size = new System.Drawing.Size(154, 20);
            this.txt_tenkhachhang.TabIndex = 13;
            // 
            // txt_DiaChi
            // 
            this.txt_DiaChi.Location = new System.Drawing.Point(98, 123);
            this.txt_DiaChi.Name = "txt_DiaChi";
            this.txt_DiaChi.Size = new System.Drawing.Size(154, 20);
            this.txt_DiaChi.TabIndex = 14;
            // 
            // txt_SDT
            // 
            this.txt_SDT.Location = new System.Drawing.Point(98, 170);
            this.txt_SDT.Name = "txt_SDT";
            this.txt_SDT.Size = new System.Drawing.Size(154, 20);
            this.txt_SDT.TabIndex = 15;
            // 
            // txt_Website
            // 
            this.txt_Website.Location = new System.Drawing.Point(344, 32);
            this.txt_Website.Name = "txt_Website";
            this.txt_Website.Size = new System.Drawing.Size(154, 20);
            this.txt_Website.TabIndex = 17;
            // 
            // txt_IDThue
            // 
            this.txt_IDThue.Location = new System.Drawing.Point(344, 80);
            this.txt_IDThue.Name = "txt_IDThue";
            this.txt_IDThue.Size = new System.Drawing.Size(154, 20);
            this.txt_IDThue.TabIndex = 18;
            // 
            // txt_sotk
            // 
            this.txt_sotk.Location = new System.Drawing.Point(344, 126);
            this.txt_sotk.Name = "txt_sotk";
            this.txt_sotk.Size = new System.Drawing.Size(154, 20);
            this.txt_sotk.TabIndex = 19;
            // 
            // txt_TenNganHang
            // 
            this.txt_TenNganHang.Location = new System.Drawing.Point(344, 173);
            this.txt_TenNganHang.Name = "txt_TenNganHang";
            this.txt_TenNganHang.Size = new System.Drawing.Size(154, 20);
            this.txt_TenNganHang.TabIndex = 20;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(344, 218);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(154, 20);
            this.txtEmail.TabIndex = 21;
            // 
            // btn__CapNhat
            // 
            this.btn__CapNhat.Location = new System.Drawing.Point(228, 254);
            this.btn__CapNhat.Name = "btn__CapNhat";
            this.btn__CapNhat.Size = new System.Drawing.Size(95, 27);
            this.btn__CapNhat.TabIndex = 22;
            this.btn__CapNhat.Text = "Cập Nhật";
            this.btn__CapNhat.UseVisualStyleBackColor = true;
            this.btn__CapNhat.Click += new System.EventHandler(this.btn__CapNhat_Click);
            // 
            // lue_KV
            // 
            this.lue_KV.Location = new System.Drawing.Point(98, 32);
            this.lue_KV.Name = "lue_KV";
            this.lue_KV.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lue_KV.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id_KhuVuc", "Id_KhuVuc"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenKhuVuc", "Tên Khu Vực")});
            this.lue_KV.Size = new System.Drawing.Size(154, 20);
            this.lue_KV.TabIndex = 23;
            // 
            // cbQuanLy
            // 
            this.cbQuanLy.AutoSize = true;
            this.cbQuanLy.Location = new System.Drawing.Point(100, 218);
            this.cbQuanLy.Name = "cbQuanLy";
            this.cbQuanLy.Size = new System.Drawing.Size(82, 17);
            this.cbQuanLy.TabIndex = 24;
            this.cbQuanLy.Text = "Còn quản lý";
            this.cbQuanLy.UseVisualStyleBackColor = true;
            // 
            // frmUpdateKH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 288);
            this.Controls.Add(this.cbQuanLy);
            this.Controls.Add(this.lue_KV);
            this.Controls.Add(this.btn__CapNhat);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txt_TenNganHang);
            this.Controls.Add(this.txt_sotk);
            this.Controls.Add(this.txt_IDThue);
            this.Controls.Add(this.txt_Website);
            this.Controls.Add(this.txt_SDT);
            this.Controls.Add(this.txt_DiaChi);
            this.Controls.Add(this.txt_tenkhachhang);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "frmUpdateKH";
            this.Text = "frmUpdateKH";
            this.Load += new System.EventHandler(this.frmUpdateKH_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lue_KV.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txt_tenkhachhang;
        private System.Windows.Forms.TextBox txt_DiaChi;
        private System.Windows.Forms.TextBox txt_SDT;
        private System.Windows.Forms.TextBox txt_Website;
        private System.Windows.Forms.TextBox txt_IDThue;
        private System.Windows.Forms.TextBox txt_sotk;
        private System.Windows.Forms.TextBox txt_TenNganHang;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Button btn__CapNhat;
        private DevExpress.XtraEditors.LookUpEdit lue_KV;
        private System.Windows.Forms.CheckBox cbQuanLy;
    }
}