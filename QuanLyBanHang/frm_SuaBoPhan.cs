﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frm_SuaBoPhan : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
        BoPhan bp = new BoPhan();
        public frm_SuaBoPhan(string id, string tenbophan, string ghichu, string trangthai)
        {
            InitializeComponent();
            LoadTextBox(id, tenbophan, ghichu, trangthai);
        }

        private void frm_SuaBoPhan_Load(object sender, EventArgs e)
        {

        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadTextBox(string id, string tenbophan, string ghichu, string trangthai)
        {
            txt_MaBoPhan.Text = id;
            txt_TenBoPhan.Text = tenbophan;
            txt_GhiChu.Text = ghichu;
            if (trangthai == "True")
            {
                cbQuanLy.Checked = true;
            };
        }
         
        private void btn_Luu_Click(object sender, EventArgs e)
        {
            int id = int.Parse(txt_MaBoPhan.Text);
            BoPhan bophan = db.BoPhans.Single(n => n.id_BoPhan == id);
            bophan.TenBoPhan = txt_TenBoPhan.Text;
            bophan.GhiChu = txt_GhiChu.Text;
            if (cbQuanLy.Checked == true)
            {
                bophan.TrangThai = true;
            }
            else
            {
                bophan.TrangThai = false;
            }
            db.SaveChangesAsync();
            MessageBox.Show("Sửa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
            this.Close();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
