﻿namespace QuanLyBanHang
{
    partial class frmNhomHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gc_NhomHang = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid_MaNhomHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_TenNhomHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_TrangThai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn_Them = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Sua = new DevExpress.XtraEditors.SimpleButton();
            this.btn_CapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Xuat = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Nhap = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Close = new DevExpress.XtraEditors.SimpleButton();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.gc_NhomHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gc_NhomHang
            // 
            this.gc_NhomHang.Location = new System.Drawing.Point(2, 32);
            this.gc_NhomHang.MainView = this.gridView1;
            this.gc_NhomHang.Name = "gc_NhomHang";
            this.gc_NhomHang.Size = new System.Drawing.Size(740, 377);
            this.gc_NhomHang.TabIndex = 0;
            this.gc_NhomHang.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid_MaNhomHang,
            this.col_TenNhomHang,
            this.col_TrangThai});
            this.gridView1.GridControl = this.gc_NhomHang;
            this.gridView1.Name = "gridView1";
            // 
            // colid_MaNhomHang
            // 
            this.colid_MaNhomHang.Caption = "Mã Nhóm Hàng";
            this.colid_MaNhomHang.FieldName = "Id_NhomHang";
            this.colid_MaNhomHang.Name = "colid_MaNhomHang";
            this.colid_MaNhomHang.Visible = true;
            this.colid_MaNhomHang.VisibleIndex = 0;
            // 
            // col_TenNhomHang
            // 
            this.col_TenNhomHang.Caption = "Tên Nhóm Hàng";
            this.col_TenNhomHang.FieldName = "TenNhomHang";
            this.col_TenNhomHang.Name = "col_TenNhomHang";
            this.col_TenNhomHang.Visible = true;
            this.col_TenNhomHang.VisibleIndex = 1;
            // 
            // col_TrangThai
            // 
            this.col_TrangThai.Caption = "Trạng Thái";
            this.col_TrangThai.FieldName = "TrangThai";
            this.col_TrangThai.Name = "col_TrangThai";
            this.col_TrangThai.Visible = true;
            this.col_TrangThai.VisibleIndex = 2;
            // 
            // btn_Them
            // 
            this.btn_Them.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Them.Location = new System.Drawing.Point(2, 0);
            this.btn_Them.Name = "btn_Them";
            this.btn_Them.Size = new System.Drawing.Size(69, 26);
            this.btn_Them.TabIndex = 3;
            this.btn_Them.Text = "Thêm";
            this.btn_Them.Click += new System.EventHandler(this.btn_Them_Click);
            // 
            // btn_Xoa
            // 
            this.btn_Xoa.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Xoa.Location = new System.Drawing.Point(90, 0);
            this.btn_Xoa.Name = "btn_Xoa";
            this.btn_Xoa.Size = new System.Drawing.Size(69, 26);
            this.btn_Xoa.TabIndex = 4;
            this.btn_Xoa.Text = "Xoá";
            this.btn_Xoa.Click += new System.EventHandler(this.btn_Xoa_Click);
            // 
            // btn_Sua
            // 
            this.btn_Sua.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Sua.Location = new System.Drawing.Point(177, 0);
            this.btn_Sua.Name = "btn_Sua";
            this.btn_Sua.Size = new System.Drawing.Size(69, 26);
            this.btn_Sua.TabIndex = 5;
            this.btn_Sua.Text = "Sửa";
            this.btn_Sua.Click += new System.EventHandler(this.btn_Sua_Click);
            // 
            // btn_CapNhat
            // 
            this.btn_CapNhat.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_CapNhat.Location = new System.Drawing.Point(264, 0);
            this.btn_CapNhat.Name = "btn_CapNhat";
            this.btn_CapNhat.Size = new System.Drawing.Size(85, 26);
            this.btn_CapNhat.TabIndex = 6;
            this.btn_CapNhat.Text = "Cập Nhật";
            this.btn_CapNhat.Click += new System.EventHandler(this.btn_CapNhat_Click);
            // 
            // btn_Xuat
            // 
            this.btn_Xuat.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Xuat.Location = new System.Drawing.Point(361, 0);
            this.btn_Xuat.Name = "btn_Xuat";
            this.btn_Xuat.Size = new System.Drawing.Size(85, 26);
            this.btn_Xuat.TabIndex = 7;
            this.btn_Xuat.Text = "Xuất";
            this.btn_Xuat.Click += new System.EventHandler(this.btn_Xuat_Click);
            // 
            // btn_Nhap
            // 
            this.btn_Nhap.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Nhap.Location = new System.Drawing.Point(461, 0);
            this.btn_Nhap.Name = "btn_Nhap";
            this.btn_Nhap.Size = new System.Drawing.Size(85, 26);
            this.btn_Nhap.TabIndex = 8;
            this.btn_Nhap.Text = "Nhập";
            // 
            // btn_Close
            // 
            this.btn_Close.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Close.Location = new System.Drawing.Point(561, 0);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(69, 26);
            this.btn_Close.TabIndex = 9;
            this.btn_Close.Text = "Đóng";
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // frmNhomHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 410);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_Nhap);
            this.Controls.Add(this.btn_Xuat);
            this.Controls.Add(this.btn_CapNhat);
            this.Controls.Add(this.btn_Sua);
            this.Controls.Add(this.btn_Xoa);
            this.Controls.Add(this.btn_Them);
            this.Controls.Add(this.gc_NhomHang);
            this.Name = "frmNhomHang";
            this.Text = "Nhóm hàng";
            this.Load += new System.EventHandler(this.frmNhomHang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gc_NhomHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gc_NhomHang;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btn_Them;
        private DevExpress.XtraEditors.SimpleButton btn_Xoa;
        private DevExpress.XtraEditors.SimpleButton btn_Sua;
        private DevExpress.XtraEditors.SimpleButton btn_CapNhat;
        private DevExpress.XtraEditors.SimpleButton btn_Xuat;
        private DevExpress.XtraEditors.SimpleButton btn_Nhap;
        private DevExpress.XtraEditors.SimpleButton btn_Close;
        private DevExpress.XtraGrid.Columns.GridColumn colid_MaNhomHang;
        private DevExpress.XtraGrid.Columns.GridColumn col_TenNhomHang;
        private DevExpress.XtraGrid.Columns.GridColumn col_TrangThai;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}