﻿namespace QuanLyBanHang
{
    partial class frmUpdateTiGia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMaTiGia = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTenTiGia = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTiGiaQuyDoi = new DevExpress.XtraEditors.TextEdit();
            this.chkTrangThaiTiGia = new System.Windows.Forms.CheckBox();
            this.btnCapNhatTiGia = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaTiGia = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaTiGia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenTiGia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiGiaQuyDoi.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "ribbonPage2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã tỉ giá :";
            // 
            // txtMaTiGia
            // 
            this.txtMaTiGia.Location = new System.Drawing.Point(114, 15);
            this.txtMaTiGia.Name = "txtMaTiGia";
            this.txtMaTiGia.Size = new System.Drawing.Size(173, 20);
            this.txtMaTiGia.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên tỉ giá :";
            // 
            // txtTenTiGia
            // 
            this.txtTenTiGia.Location = new System.Drawing.Point(114, 51);
            this.txtTenTiGia.Name = "txtTenTiGia";
            this.txtTenTiGia.Size = new System.Drawing.Size(173, 20);
            this.txtTenTiGia.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tỉ giá quy đổi :";
            // 
            // txtTiGiaQuyDoi
            // 
            this.txtTiGiaQuyDoi.Location = new System.Drawing.Point(114, 89);
            this.txtTiGiaQuyDoi.Name = "txtTiGiaQuyDoi";
            this.txtTiGiaQuyDoi.Size = new System.Drawing.Size(173, 20);
            this.txtTiGiaQuyDoi.TabIndex = 1;
            // 
            // chkTrangThaiTiGia
            // 
            this.chkTrangThaiTiGia.AutoSize = true;
            this.chkTrangThaiTiGia.Location = new System.Drawing.Point(210, 118);
            this.chkTrangThaiTiGia.Name = "chkTrangThaiTiGia";
            this.chkTrangThaiTiGia.Size = new System.Drawing.Size(76, 17);
            this.chkTrangThaiTiGia.TabIndex = 2;
            this.chkTrangThaiTiGia.Text = "Hoạt động";
            this.chkTrangThaiTiGia.UseVisualStyleBackColor = true;
            // 
            // btnCapNhatTiGia
            // 
            this.btnCapNhatTiGia.ImageUri.Uri = "Add;Size16x16;Colored";
            this.btnCapNhatTiGia.Location = new System.Drawing.Point(12, 149);
            this.btnCapNhatTiGia.Name = "btnCapNhatTiGia";
            this.btnCapNhatTiGia.Size = new System.Drawing.Size(113, 32);
            this.btnCapNhatTiGia.TabIndex = 3;
            this.btnCapNhatTiGia.Text = "Cập nhật";
            this.btnCapNhatTiGia.Click += new System.EventHandler(this.btnCapNhatTiGia_Click);
            // 
            // btnXoaTiGia
            // 
            this.btnXoaTiGia.ImageUri.Uri = "Cancel";
            this.btnXoaTiGia.Location = new System.Drawing.Point(144, 149);
            this.btnXoaTiGia.Name = "btnXoaTiGia";
            this.btnXoaTiGia.Size = new System.Drawing.Size(142, 31);
            this.btnXoaTiGia.TabIndex = 4;
            this.btnXoaTiGia.Text = "Đóng";
            this.btnXoaTiGia.Click += new System.EventHandler(this.btnXoaTiGia_Click);
            // 
            // frmUpdateTiGia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 193);
            this.Controls.Add(this.btnXoaTiGia);
            this.Controls.Add(this.btnCapNhatTiGia);
            this.Controls.Add(this.chkTrangThaiTiGia);
            this.Controls.Add(this.txtTiGiaQuyDoi);
            this.Controls.Add(this.txtTenTiGia);
            this.Controls.Add(this.txtMaTiGia);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmUpdateTiGia";
            this.Text = "Cập nhật tỉ giá";
            this.Load += new System.EventHandler(this.frmUpdateTiGia_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtMaTiGia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenTiGia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTiGiaQuyDoi.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txtMaTiGia;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit txtTenTiGia;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit txtTiGiaQuyDoi;
        private System.Windows.Forms.CheckBox chkTrangThaiTiGia;
        private DevExpress.XtraEditors.SimpleButton btnCapNhatTiGia;
        private DevExpress.XtraEditors.SimpleButton btnXoaTiGia;
    }
}