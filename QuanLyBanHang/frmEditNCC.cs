﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmEditNCC : Form
    {
        DACK_ware5Entities1 dbe = new DACK_ware5Entities1();
        public frmEditNCC(string id, string id_kv, string TenNCC, string ChucVu, string DiaChi, string SDT, string TrangThai)
        {
            InitializeComponent();
            LoadComboBox();
            LoadTextBox(id, id_kv, TenNCC, ChucVu, DiaChi, SDT, TrangThai);
        }

        private void frmEditNCC_Load(object sender, EventArgs e)
        {

        }
        private void LoadComboBox()
        {
            var a = dbe.KhuVucs.ToList();

            lue_KV.Properties.DataSource = a.ToList();
            //lue_KV.Properties.DisplayMember = "TenKhuVuc";
            lue_KV.Properties.DisplayMember = "Id_KhuVuc";
            lue_KV.Properties.ValueMember = "Id_KhuVuc";
            //ValueMember

        }
        private void LoadTextBox(string id, string id_kv, string TenNCC, string ChucVu, string DiaChi, string SDT,  string TrangThai)
        {
            txt_IDNCC.Text = id;
            lue_KV.Text = id_kv;
            txt_TenNCC.Text = TenNCC;
            txt_CV.Text = ChucVu;
            txt_DiaChi.Text = DiaChi;
            txt_SDT.Text = SDT;
            if (TrangThai == "True")
            { cbQuanLy.Checked = true; }

        }
        private void btn_Luu_Click(object sender, EventArgs e)
        {
            int id = int.Parse(txt_IDNCC.Text);
            NhaCungCap NCC = dbe.NhaCungCaps.Single(n => n.id_NCC == id);
            string m = lue_KV.EditValue.ToString();
            NCC.TenNCC = txt_TenNCC.Text;
            NCC.SDT = txt_SDT.Text;
            NCC.ChucVu = txt_CV.Text;
            NCC.DiaChi = txt_DiaChi.Text;
            if (cbQuanLy.Checked == true)
            { NCC.TrangThai = true; }
            else
            {
                NCC.TrangThai = false;
            }
            dbe.SaveChangesAsync();
            MessageBox.Show("Sửa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
            this.Close();
        }
    }
}
